""" A wrapper around open reid."""

from typing import List, Dict
import reid.models
from reid.utils.serialization import load_checkpoint
from reid.dist_metric import DistanceMetric
from reid.utils.data import transforms as T
from reid.utils import to_torch
from torch import nn
from torch.utils.data import DataLoader
from torch.autograd import Variable
from PIL import Image
from tqdm import tqdm
from collections import namedtuple
from ...extractor import AoiExtractor
from ...shape import Box
import numpy as np

Aoi = namedtuple('Aoi', 'image_file, aoi')


class OpenReidExtractor(AoiExtractor):
    def __init__(self, imgs: List[str],
                 aois: List[Box],
                 architecture: str,
                 checkpoint: str,
                 state_dict: str,
                 batch_size: int):
        super().__init__(imgs, aois)
        self._model = reid.models.create(architecture,
                                         num_features=1024,
                                         dropout=0,
                                         num_classes=128)

        self._model.load_state_dict(load_checkpoint(checkpoint)['state_dict'])
        self._model = nn.DataParallel(self.__model).cuda()
        self._model.eval()
        self._metric = DistanceMetric()
        self._arch = architecture
        self._batch_size = batch_size

    def get_descriptors(self) -> List[Dict]:
        """Return the descriptor corresponding with the AOI in the image."""
        normalizer = T.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])

        height, width = ((144, 56) if self.__arch == 'inception'
                         else (256, 128))

        transformer = T.Compose([
            T.RectScale(height, width),
            T.ToTensor(),
            normalizer,
        ])

        data_loader = DataLoader(Preprocessor(self._img_paths, transformer,
                                              self._aois),
                                 batch_size=self.__batch_size)

        descriptors = []
        for imgs, img_files, aois in tqdm(data_loader):
            inputs = to_torch(imgs)
            inputs = Variable(inputs, volatile=True)
            outputs = self.__model(inputs)
            outputs = outputs.data.cpu()

            # !! aois is a list of torch tensors
            # each torch tensor corresponds to either
            # x_min/x_max/y_min/y_max of all aois
            np_aois = np.asarray([list(x) for x in aois]).T

            for img_file, output, aoi in zip(img_files, outputs, np_aois):
                descriptors.append({'image': img_file,
                                    'descriptor': output.numpy(),
                                    'aoi': Box(x_min=aoi[0],
                                               y_min=aoi[1],
                                               x_max=aoi[2],
                                               y_max=aoi[3])})

        return descriptors


class Preprocessor(object):
    def __init__(self, image_files, transform=None,
                 areas_of_interest=None):
        super(Preprocessor, self).__init__()
        self.image_files = image_files
        self.transform = transform

        if areas_of_interest is not None:
            self.aois = []
            for image_file, aois in areas_of_interest.items():
                self.aois.extend([Aoi(image_file=image_file,
                                      aoi=aoi)
                                  for aoi in aois])
        else:
            self.aois = None

    def __len__(self):
        if self.aois is not None:
            return len(self.aois)
        else:
            return len(self.image_files)

    def __getitem__(self, indices):
        if isinstance(indices, (tuple, list)):
            return [self.__get_single_item(i) for i in indices]
        return self.__get_single_item(indices)

    def __get_single_item(self, index):
        if self.aois is not None:
            image_file = self.aois[index].image_file
            aoi = self.aois[index].aoi
            img = Image.open(image_file).convert('RGB')
            img = img.crop((aoi.x_min,  # left
                            aoi.y_min,  # upper
                            aoi.x_max,  # right
                            aoi.y_max))  # lower
        else:
            image_file = self.image_files[index]
            img = Image.open(image_file).convert('RGB')
            aoi = Box(x_min=0, y_min=0,
                      x_max=img.width, y_max=img.height)

        if self.transform is not None:
            img = self.transform(img)

        return img, image_file, aoi
