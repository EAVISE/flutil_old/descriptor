"""Module providing an implementation with an mxnet backend."""
from typing import List
from collections import namedtuple
import logging
import mxnet as mx
from mxnet.module.base_module import BaseModule
import numpy as np
from ..extractor import Extractor, DetectionExtractor
from ...detector import Detection

Batch = namedtuple('Batch', ['data'])


class MxModExtractor(Extractor):
    """An ``Extractor`` using trained mxnet models.

    :param Module model: the mxnet model to use.
    :param List[str] img_paths: the image paths from which a descriptor needs
    to be extracted.
    :param transform: callable that tranforms an image into the desired format
    to feed into the module.
    """
    def __init__(self, model: BaseModule, img_paths: List[str],
                 transform):
        super().__init__(img_paths)

        self.model = model
        self.transform = transform

    def _get_descriptor(self, img: np.array) -> np.array:
        img = mx.nd.array(img)
        img = self.transform(img)
        self.model.forward(Batch([img]), is_train=False)
        return self.model.get_outputs()[0].asnumpy().flatten()


class MxModDetExtractor(DetectionExtractor, MxModExtractor):
    """An extractor for detections."""
    def __init__(self, model: BaseModule, img_paths: List[str],
                 dets: List[List[Detection]], transform=None):
        MxModExtractor.__init__(self, model, img_paths, transform)
        DetectionExtractor.__init__(self, img_paths, dets)

    def _get_detection_descriptor(self,
                                  img: np.array,
                                  detection: Detection) -> np.array:
        if img.ndim != 3:
            logging.warning('Image has ndim != 3')
            return None

        detection = detection.to_int()
        height, width, _ = img.shape
        detection.x_min = max(detection.x_min, 0)
        detection.x_max = min(detection.x_max, width)
        detection.y_min = max(detection.y_min, 0)
        detection.y_max = min(detection.y_max, height)
        return self._get_descriptor(img[detection.y_min:detection.y_max,
                                        detection.x_min:detection.x_max,
                                        :])

    def _get_descriptor(self, img: np.array) -> np.array:
        return MxModExtractor._get_descriptor(self, img)
