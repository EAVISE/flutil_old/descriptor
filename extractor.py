import pandas as pd
from typing import List, Dict
from ..shape import Box
from tqdm import tqdm
from skimage import io
from ..detector import Detection
import numpy as np


class Extractor:
    """An extractor that creates a descriptor from an image."""

    def __init__(self, imgs: List[str]):
        """
        :param List[str] imgs: the image paths
        """
        self.imgs = imgs

    def _get_descriptor(self, im: np.array) -> np.array:
        """Return the descriptor for an image.

        :param np.array im: the image array obtained by calling
        ``skimage.io.imread()`` with the image file path as an argument.
        """
        raise NotImplementedError

    def __iter__(self):
        for img in self.imgs:
            im = io.imread(img)
            yield self._get_descriptor(im)

    def __getitem__(self, i):
        im = io.imread(self.imgs[i])
        return self._get_descriptor(im)

    def __len__(self):
        return len(self.imgs)

    def get_descriptors(self) -> List[Dict]:
        """Return a list of dicts with all the descriptors for all the images.

        :returns: a list of dicts with all the descriptors for all the images
        :rtype: List[Dict]
        """
        return [{'descriptor': desc,
                 'image': img_path}
                for img_path, desc in tqdm(zip(self.imgs, self),
                                           total=len(self))]

    def get_descriptors_df(self) -> pd.DataFrame:
        """Return a `DataFrame` with all the descriptors for all the images.

        :returns: a pandas DataFrame with the image and the descriptor
        :rtype: pd.DataFrame
        """
        descriptors = self.get_descriptors()
        return pd.DataFrame(descriptors)


class DetectionExtractor(Extractor):
    """An extractor that creates a descriptor from a detection region within an
    image."""

    def __init__(self, imgs: List[str],
                 dets: List[List[Detection]]):
        """
        :param List[str] imgs: the image paths
        :param List[List[Detection]] dets: the detections per image
        """
        if len(imgs) != len(dets):
            raise ValueError('Lengths of imgs and dets must be '
                             'equal.')

        # Flatten the detection list and expand the image list along with it
        zipped = [(img, det)
                  for i, img in enumerate(imgs)
                  for det in dets[i]]
        self.imgs, self.dets = [list(t) for t in zip(*zipped)]

        assert len(self.imgs) == len(self.dets)

    def _get_detection_descriptor(self,
                                  im: np.array,
                                  detection: Detection) -> np.array:
        """Return the descriptor corresponding with the detection region in the
        image."""
        raise NotImplementedError

    def __iter__(self):
        for (img, det) in zip(self.imgs, self.dets):
            im = io.imread(img)
            yield self._get_detection_descriptor(im, det)

    def __getitem__(self, i):
        im = io.imread(self.imgs[i])
        det = self.dets[i]
        return self._get_detection_descriptor(im, det)

    def __len__(self):
        assert len(self.imgs) == len(self.dets)
        return len(self.imgs)

    def get_descriptors(self) -> List[Dict]:
        """Return the descriptors for all images and their detections.

        Note that images without a detection will be excluded.

        :returns: a list of dicts with all the descriptors for all the images
        and their detections
        :rtype: List[Dict]
        """
        return [{'detection': det,
                 'descriptor': descr,
                 'image': img}
                for img, det, descr in tqdm(zip(self.imgs, self.dets, self),
                                            total=len(self))]


class AoiExtractor(Extractor):
    """An extractor that creates a descriptor from an AOI within an image."""

    def __init__(self, imgs: List[str],
                 aois: List[List[Box]]):
        """
        :param List[str] imgs: the image paths
        :param List[List[Box]] aois: the areas of interest (AOIs) for each
        image
        """
        if not all([a is not None for a in aois]):
            # None of the aois elements should be None!
            raise ValueError('The AOIs should be not None for each image. '
                             'If an image has no AOIs, use an empty list '
                             'instead, or set the AOI to contain the whole '
                             'image.')
        if not len(imgs) == len(aois):
            # Each set of AOIs correspond with an image at the same index
            raise ValueError('imgs and aois must be lists of the same '
                             'length.')

        # Set empty list AOIs to the images width and height
        xtra_aois = []
        for img_path, img_aois in zip(imgs, aois):
            if len(img_aois) == 0:
                im = io.imread(img_path)
                # Use the full image area as AOI
                aoi = Box.from_width_height(im.shape[0], im.shape[1])
                xtra_aois.append([aoi])
            else:
                xtra_aois.append(img_aois)

        # Flatten the AOI list and expand the image list along with it
        zipped = [(img, aoi)
                  for i, img in enumerate(imgs)
                  for aoi in xtra_aois[i]]
        self.imgs, self.aois = [list(t) for t in zip(*zipped)]

    def _get_aoi_descriptor(self, im: np.array, aoi: Box) -> np.array:
        """Return the descriptor corresponding with the AOI in the image."""
        aoi = aoi.to_int()
        img_aoi = im[aoi.y_min:aoi.y_max, aoi.x_min:aoi.x_max]
        return self._get_descriptor(img_aoi)

    def __iter__(self):
        for (img, aoi) in zip(self.imgs, self.aois):
            im = io.imread(img)
            yield self._get_aoi_descriptor(im, aoi)

    def __getitem__(self, i):
        im = io.imread(self.imgs[i])
        aoi = self.aois[i]
        return self._get_aoi_descriptor(im, aoi)

    def __len__(self):
        assert len(self.imgs) == len(self.aois)
        return len(self.imgs)

    def get_descriptors(self) -> List[Dict]:
        """Return the descriptors for all images and their AOIs.

        :returns: a list of dicts with AOI box and descriptor
        :rtype: List[Dict]
        """
        return [{'aoi': aoi,
                 'descriptor': descr,
                 'image': img}
                for img, aoi, descr in tqdm(zip(self.imgs, self.aois, self),
                                            total=len(self))]


class AoiDetectionExtractor(AoiExtractor, DetectionExtractor):
    """An extractor that creates a descriptor from a detection region within an
    AOI within an image."""

    def __init__(self, imgs: List[str],
                 dets: List[List[Detection]],
                 aois: List[List[Box]]):
        """
        A detection entirely overlap with an AOI to be considered part of an
        AOI.

        :param List[str] imgs: the image paths
        :param List[List[Detection]] dets: the detections per image
        :param List[List[Box]] aois: the areas of interest (AOIs) for each
        image
        """
        if not all([a is not None for a in aois]):
            # None of the aois elements should be None!
            raise ValueError('The AOIs should be not None for each image. '
                             'If an image has no AOIs, use an empty list '
                             'instead, or set the AOI to contain the whole '
                             'image.')
        if len(imgs) != len(aois):
            # Each set of AOIs correspond with an image at the same index
            raise ValueError('imgs and aois must be lists of the same '
                             'length.')
        if len(imgs) != len(dets):
            raise ValueError('Lengths of imgs and dets must be '
                             'equal.')

        # Set empty list AOIs to the images width and height
        xtra_aois = []
        for img_path, img_aois in zip(imgs, aois):
            if len(img_aois) == 0:
                im = io.imread(img_path)
                # Use the full image area as AOI
                aoi = Box.from_width_height(im.shape[0], im.shape[1])
                xtra_aois.append([aoi])
            else:
                xtra_aois.append(img_aois)

        zipped = [(img, aoi, det)
                  for i, img in enumerate(imgs)
                  for aoi in xtra_aois[i]
                  for det in dets[i]
                  if det in aoi]  # Check if detection is inside AOI

        self.imgs, self.aois, self.dets = [list(t) for t in zip(*zipped)]

    def _get_aoidet_descriptor(self, im: np.array, aoi: Box,
                               det: Detection) -> List[Dict]:
        """
        Return the descriptor corresponding with the AOI and detection in
        the image.
        """
        aoi = aoi.to_int()
        aoi_img = im[aoi.y_min:aoi.y_max, aoi.x_min:aoi.x_max]
        aoi_det = det - aoi  # Shift detection into AOI coordinates
        self._get_detection_descriptor(aoi_img, aoi_det)

    def __iter__(self):
        for (img, aoi, det) in zip(self.imgs, self.aois, self.dets):
            im = io.imread(img)
            yield self._get_aoidet_descriptor(im, aoi, det)

    def __getitem__(self, i):
        im = io.imread(self.imgs[i])
        aoi = self.aois[i]
        det = self.dets[i]
        return self._get_aoidet_descriptor(im, aoi, det)

    def __len__(self):
        assert len(self.imgs) == len(self.aois)
        assert len(self.imgs) == len(self.dets)
        return len(self.imgs)

    def get_descriptors(self) -> List[Dict]:
        """Return the descriptors for all images and their AOIs.

        :returns: a list of dicts with AOI box and descriptor
        :rtype: List[Dict]
        """
        return [{'aoi': aoi,
                 'descriptor': descr,
                 'detection': det,
                 'image': img}
                for img, aoi, det, descr in tqdm(zip(self.imgs, self.aois,
                                                     self.dets, self),
                                                 total=len(self))]
