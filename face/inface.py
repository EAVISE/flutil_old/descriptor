import os.path
import numpy as np
import mxnet as mx
from typing import List
from ..mxmod import MxModDetExtractor
import logging
from ...detector import BoxShapeDetection
from ...detector import ShapeDetection, Detection
from sklearn.preprocessing import normalize
from skimage import transform
import cv2

IMAGE_INPUT_SIZE = (112, 112)

MODEL_PREFIX = os.path.join(os.path.dirname(__file__),
                            'etc',
                            'model-r100-ii/model')


class InfaceExtractor(MxModDetExtractor):
    """Facial feature extrator based on InsightFace.

    See: https://github.com/deepinsight/insightface
    """
    def __init__(self, img_paths: List[str],
                 dets: List[List[Detection]],
                 model_prefix: str = None):
        """
        :param str model_prefix: (optional) mxnet model prefix
        """
        if model_prefix is None:
            prefix = MODEL_PREFIX
        else:
            prefix = model_prefix

        epoch = 0
        ctx = mx.gpu(0)
        batch_size = 1  # Should be higher than the number of GPUs
        sym, arg_params, aux_params = mx.model.load_checkpoint(prefix, epoch)
        all_layers = sym.get_internals()
        sym = all_layers['fc1_output']
        mod = mx.mod.Module(symbol=sym, context=ctx, label_names=None)
        mod.bind(data_shapes=[('data', (batch_size, 3, 112, 112))])
        mod.set_params(arg_params, aux_params)

        def transform(img):
            img = mx.image.imresize(img, 112, 112)  # resize
            img = img.transpose((2, 0, 1))  # Channel first
            img = img.expand_dims(axis=0)  # batchify
            return img

        super().__init__(mod, img_paths, dets, transform)

    @staticmethod
    def _landmark_warp(img, det: ShapeDetection):
        src = np.array([[30.2946, 51.6963],
                        [65.5318, 51.5014],
                        [48.0252, 71.7366],
                        [33.5493, 92.3655],
                        [62.7299, 92.2041]],
                       dtype=np.float32)
        src[:, 0] += 8.0

        dst = np.array([list(p) for p in det.points],
                       dtype=np.float32)
        tform = transform.SimilarityTransform()
        tform.estimate(dst, src)
        M = tform.params[0:2, :]
        warped = cv2.warpAffine(img, M, IMAGE_INPUT_SIZE)
        return warped

    @staticmethod
    def _crop_box(img, det: Detection, margin=44):
        height, width, _ = img.shape
        det.x_min = max(det.x_min - margin/2, 0)
        det.y_min = min(det.y_min + margin/2, 0)
        det.x_max = max(det.x_max - margin/2, width)
        det.y_max = min(det.y_max + margin/2, height)
        det = det.to_int()

        return img[det.y_min:det.y_max, det.x_min:det.x_max, :]

    def _get_detection_descriptor(self,
                                  img: np.array,
                                  detection: Detection) -> np.array:
        if img.ndim != 3:
            logging.warning('Image has ndim != 3')
            return None

        if isinstance(detection, BoxShapeDetection):
            img = self._landmark_warp(img, detection.points)
        elif isinstance(detection, ShapeDetection):
            img = self._landmark_warp(img, detection)
        else:
            img = self._crop_box(img, detection)

        # Get descriptor for the face and the flipped version of the face
        descr_normal = self._get_descriptor(img)
        descr_flipped = self._get_descriptor(img[:, ::-1, :])

        # Combine and normalize the descriptors
        descr = descr_normal + descr_flipped
        return normalize(descr[np.newaxis, :]).flatten()
