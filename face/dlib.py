"""A wrapper around the dlib face recognition."""
import dlib
from ..extractor import DetectionExtractor, AoiDetectionExtractor
from ...detector import Detection
import os
import numpy as np
import logging
from typing import List


SHAPE_PRED_PATH = os.path.join(os.path.dirname(__file__),
                               'etc',
                               'shape_predictor_5_face_landmarks.dat')
FACE_MODEL_PATH = os.path.join(os.path.dirname(__file__),
                               'etc',
                               'face_recognition_resnet_model_v1.dat')


class DlibFaceExtractor(DetectionExtractor):
    """DLIB implementation of face Descriptor."""

    def __init__(self, img_paths: List[str], dets: List[List[Detection]]):
        super().__init__(img_paths, dets)
        self._face_rec = dlib.face_recognition_model_v1(FACE_MODEL_PATH)
        self._shape_pred = dlib.shape_predictor(SHAPE_PRED_PATH)

    def _get_detection_descriptor(self,
                                  img: np.array,
                                  detection: Detection) -> np.array:
        if img.ndim != 3:
            logging.warning('Image has ndim != 3')
            return None

        det = dlib.rectangle(left=detection.left,
                             right=detection.right,
                             top=detection.top,
                             bottom=detection.bottom)
        shape = self._shape_pred(img, det)
        return np.array(self._face_rec.compute_face_descriptor(img, shape))


class DlibFaceAoiExtractor(AoiDetectionExtractor, DlibFaceExtractor):
    pass
