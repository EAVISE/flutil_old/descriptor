"""A wrapper around the face_recognition from ageitgey."""

from ..extractor import AoiDetectionExtractor
from ...detector import Detection
from face_recognition import api
import numpy as np
import logging


class GeitgeyFaceExtractor(AoiDetectionExtractor):
    """Adam Geitgey implementation of face recognition."""

    def _get_detection_descriptor(self,
                                  img: np.array,
                                  detection: Detection) -> np.array:
        # Bounding box in CSS order (top, right, bottom, left)
        encodings = api.face_encodings(img, [detection.css()])
        try:
            return encodings[0]
        except IndexError as e:
            logging.error(f'It was impossible to encode detection {detection} '
                          f'from image with shape {img.shape}.')
