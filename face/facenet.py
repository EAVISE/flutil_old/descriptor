"""A wrapper around facenet"""
import os
import sys
from ..extractor import AoiDetectionExtractor
from ...shape import Box
from ...detector import Detection
from typing import List
import numpy as np
from scipy import misc
sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'etc'))
from facenet.contributed import face


class FacenetExtractor(AoiDetectionExtractor):
    """Facenet implementation of a face Descriptor."""

    def __init__(self, img_paths: List[str],
                 dets: List[List[Detection]],
                 aois: List[List[Box]]):
        super().__init__(img_paths, dets, aois)
        self._encoder = face.Encoder()

    def _get_detection_descriptor(self,
                                  img: np.array,
                                  detection: Detection) -> np.array:
        # Make sure it is RGB
        if img.ndim != 3:
            return None

        img_face = face.Face()
        img_face.container_image = img
        img_face.bounding_box = np.zeros(4, dtype=np.int32)

        img_face.bounding_box[0] = int(detection.x_min)
        img_face.bounding_box[1] = int(detection.y_min)
        img_face.bounding_box[2] = int(detection.x_max)
        img_face.bounding_box[3] = int(detection.y_max)

        cropped = img[detection.y_min:detection.y_max,
                      detection.x_min:detection.x_max]
        img_face.image = misc.imresize(cropped,
                                       (self.detect.face_crop_size,
                                        self.detect.face_crop_size),
                                       interp='bilinear')

        return self._encoder.generate_embedding(img_face)
