"""Use face angle estimation from Dries Hulens."""
from ..extractor import AoiExtractor, Extractor
import numpy as np
from ...shape import Box
import cv2 as cv
import os.path

FRONTAL_HAAR = os.path.join(os.path.dirname(__file__),
                            'etc', 'haarcascade_frontalface_default.xml')
PROFILE_HAAR = os.path.join(os.path.dirname(__file__),
                            'etc', 'haarcascade_profileface.xml')


class DhuAngleExtractor(Extractor):
    """
    Extractor for head angles, based on a method by Dries Hulens.

    This ``Extractor`` just averages the amount of profile and frontal face
    detections in each image. A single head angle estimation will be returned,
    even when multiple faces are present in the image. If you would like to do
    head angle estimations within annotated or detected regions, you should use
    the ``DhuAoiAngleExtractor`` instead.
    """
    def _detect(self, img, dets, flip=False):
        if flip:
            img = cv.flip(img, 1)
        return [Box(x_min=d[0],
                    y_min=d[1],
                    x_max=(d[0]+d[2]),
                    y_max=(d[1]+d[3]))
                for d in dets]

    def _estimate_angle(self, front_dets, left_dets, right_dets):
        n_left = len(left_dets)
        n_right = len(right_dets)
        n_front = len(front_dets)

        numer = 90*n_left - 90*n_right
        denom = n_left + n_front + n_right
        if denom != 0:
            return numer/denom
        else:
            return None

    def _get_descriptor(self, img: np.array) -> np.array:
        frontal_detector = cv.CascadeClassifier(FRONTAL_HAAR)
        profile_detector = cv.CascadeClassifier(PROFILE_HAAR)
        front_dets = self._detect(img, frontal_detector)
        left_dets = self._detect(img, profile_detector)
        right_dets = self._detect(img, profile_detector, flip=True)

        yaw = self._estimate_angle(front_dets, left_dets, right_dets)

        return np.array([yaw])


class DhuAoiAngleExtractor(DhuAngleExtractor, AoiExtractor):
    """
    Extractor for head angles, based on a method by Dries Hulens.

    As opposed to the ``DhuAngleExtractor``, this ``Extractor`` only estimates
    head angles within a selected region in an image. Not that the head should
    be well inside the given areas of interest as our underlying face detectors
    would have a difficult time detecting an incomplete face.
    """
    def _get_aoi_descriptor(self, img: np.array, aoi: Box) -> np.array:
        aoi_img = img[aoi.y_min:aoi.y_max, aoi.x_min:aoi.x_max]
        return self._get_descriptor(aoi_img)
