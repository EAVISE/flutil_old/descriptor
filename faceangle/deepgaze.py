"""A wrapper around DeepGaze gaze estimation."""

from ..extractor import AoiDetectionExtractor
from ...detector import Detection
import os
import tensorflow as tf
import numpy as np
from deepgaze.head_pose_estimation import CnnHeadPoseEstimator
from deepgaze.face_detection import HaarFaceDetector
from ...shape import Box
from typing import List
import logging

ROLL_VARIABLES = os.path.join(os.path.dirname(__file__),
                              'etc', 'roll_cnn_cccdd_30k.tf')
PITCH_VARIABLES = os.path.join(os.path.dirname(__file__),
                               'etc', 'pitch_cnn_cccdd_30k.tf')
YAW_VARIABLES = os.path.join(os.path.dirname(__file__),
                             'etc', 'yaw_cnn_cccdd_30k.tf')
FRONT_FACE = os.path.join(os.path.dirname(__file__),
                          'etc', 'haarcascade_frontalface_alt.xml')
PROFILE_FACE = os.path.join(os.path.dirname(__file__),
                            'etc', 'haarcascade_profileface.xml')


class DeepGazeExtractor(AoiDetectionExtractor):
    """DeepGaze implementation of Extractor."""
    def __init__(self, *args, imgs: List[str],
                 dets: List[Detection],
                 aois: List[Box], **kwargs):
        super().__init__(imgs, dets, aois)
        self._sess = tf.Session()
        self._head_pose_estimator = CnnHeadPoseEstimator(self._sess)
        self._head_pose_estimator.load_roll_variables(ROLL_VARIABLES)
        self._head_pose_estimator.load_pitch_variables(PITCH_VARIABLES)
        self._head_pose_estimator.load_yaw_variables(YAW_VARIABLES)
        self._face_detector = HaarFaceDetector(FRONT_FACE, PROFILE_FACE)

    def _get_detection_descriptor(self,
                                  img: np.array,
                                  detection: Detection) -> np.array:
        """Return the descriptor corresponding with the detection region in the
        image."""
        det_img = img[detection.y_min:detection.y_max,
                      detection.x_min:detection.x_max]

        # Image must be RGB and >= 64 x 64 pixels
        if (det_img.ndim != 3
                or det_img.shape[0] < 64
                or det_img.shape[1] < 64):
            logging.warn('A detection with shape {det_img.shape} was passed, '
                         'but detection shapes should be >= 64 x 64.')
            return []
        try:
            roll = self._head_pose_estimator.return_roll(det_img)
            pitch = self._head_pose_estimator.return_pitch(det_img)
            yaw = self._head_pose_estimator.return_yaw(det_img)
            return np.array([roll[0, 0, 0],  pitch[0, 0, 0],  yaw[0, 0, 0]])
        except ValueError as e:
            logging.error(e.msg)
            return []
