"""Use facial landmarks to estimate gaze direction."""

from ..extractor import AoiDetectionExtractor
from ...detector import ShapeDetection
from ...shape import Box
from typing import List
import json
import cv2
import numpy as np
import math
import os.path
import logging

HEAD_POINTS = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                           'etc', '3D_human_head_openpose_order.json')


class LandmarkGazeExtractor(AoiDetectionExtractor):
    """Implementation of a gaze Descriptor using facial landmarks."""
    def __init__(self, imgs: List[str], dets: List[List[ShapeDetection]],
                 aois: List[Box], min_prob: float = 0.5, num_points: int = 6,
                 head_points_file: str = HEAD_POINTS):
        """
        :param List[str] imgs: the image paths
        :param PointsDetector detector: performs the detection required before
        a feature can be extracted
        :param List[List[Box]] aois: the areas of interest (AOIs) for each
        image
        :param float min_prob: least confidence required for a keypoint
        detection to be considered valid (default 0.5)
        :param int num_points: the minimum amount of keypoints to match with
        the 3d model (default 6)
        :param str head_points_file: path of a json file containing a list of
        coordinates corresponding with keypoints (default
        './etc/3D_human_head_openpose_order.json')
        :param int nose_index: the index of the nose in a list of keypoints
        """
        super().__init__(imgs, dets, aois)

        self._min_prob = min_prob
        self._num_points = num_points

        with open(head_points_file) as f:
            kps = json.load(f)
        self._3d_points = np.array(kps)

    def _get_face_pose(self,
                       detection: ShapeDetection, img_shape: tuple):
        # Camera internals
        focal_length = img_shape[1]
        center = (img_shape[1]/2, img_shape[0]/2)
        camera_matrix = np.array([[focal_length, 0, center[0]],
                                  [0, focal_length, center[1]],
                                  [0, 0, 1]], dtype='double')

        dist_coeffs = np.zeros((4, 1))

        probs = np.array([l.prob for l in detection])
        mask = probs > self.min_prob
        if mask.sum() < self.num_points:
            # There are not enough points with high confidence,
            # so pose estimation will be useless
            return None

        # idx = np.argsort(probs)[::-1][:self.num_points]
        idx = np.arange(mask.size)[mask]
        _2d_points = np.array([(l.x, l.y) for l in detection])
        _2d_points = _2d_points[idx]
        _3d_points = self._3d_points[idx]

        # Initializing the head pose 1m away, roughly facing the robot
        # This initialization is important as it prevents solvePnP to find the
        # mirror solution (head *behind* the camera)
        tvec = np.array([0., 0., 1.])
        rvec = np.array([1.2, 1.2, -1.2])

        # Find the 3D pose of our head
        cv2.solvePnPRansac(_3d_points, _2d_points,
                           camera_matrix, dist_coeffs,
                           rvec, tvec, True,
                           flags=cv2.SOLVEPNP_ITERATIVE | cv2.SOLVEPNP_UPNP)

        # rvec and tvec bring points from the model coordinate system
        # to the camera coordinate system

        rotation = np.zeros((3, 3))
        cv2.Rodrigues(rvec, rotation)

        pose = np.block([[rotation,             tvec[:, np.newaxis]],
                         [np.zeros((1, 3)),     1]])

        return np.array(pose)

    @staticmethod
    def _convert_to_degrees(angle):
        angle = angle*180/math.pi
        angle = angle % 360
        angle = angle - 360 if angle > 180 else angle
        return angle

    @staticmethod
    def _get_roll_pitch_yaw_gazr(R):
        """Return yaw pitch roll from rotation matrix.

        This code was based on function getEulerYPR from
        https://github.com/severin-lemaignan/gazr
        /blob/master/tools/LinearMath/Matrix3x3.h

        :param np.array R: the rotation matrix
        """
        # Check that pitch is not at a singularity
        if R[2, 0] >= 1:
            yaw = 0

            #  From difference of angles formula
            if R[2, 0] < 0:  # gimbal locked down
                delta = math.atan2(R[0, 1], R[0, 2])
                pitch = math.pi / 2.0
                roll = delta

            else:  # gimbal locked up
                delta = math.atan2(-R[0, 1], -R[0, 2])
                pitch = -math.pi / 2.0
                roll = delta

        else:
            pitch = - math.asin(R[2, 0])
            roll = math.atan2(R[2, 1]/math.cos(pitch),
                              R[2, 2]/math.cos(pitch))
            yaw = math.atan2(R[1, 0]/math.cos(pitch),
                             R[0, 0]/math.cos(pitch))

        # Assign to user's reference
        user_yaw = -pitch
        user_pitch = roll - math.pi
        user_roll = yaw

        user_roll, user_pitch, user_yaw = [LandmarkGazeExtractor
                                           ._convert_to_degrees(angle)
                                           for angle in [user_roll,
                                                         user_pitch,
                                                         user_yaw]]

        return np.array([user_roll, user_pitch, user_yaw])

    @staticmethod
    def _get_roll_pitch_yaw_locv(R):
        """Return yaw pitch roll from rotation matrix.

        This code was based on
        https://www.learnopencv.com/rotation-matrix-to-euler-angles/

        :param np.array R: the rotation matrix
        """
        sy = math.sqrt(R[0, 0] * R[0, 0] + R[1, 0] * R[1, 0])

        singular = sy < 1e-6

        if not singular:
            roll = math.atan2(R[2, 1], R[2, 2])
            pitch = math.atan2(-R[2, 0], sy)
            yaw = math.atan2(R[1, 0], R[0, 0])
        else:
            roll = math.atan2(-R[1, 2], R[1, 1])
            pitch = math.atan2(-R[2, 0], sy)
            yaw = 0

        # Assign to user's reference
        user_yaw = -pitch
        user_pitch = roll - math.pi
        user_roll = yaw
        return np.array([LandmarkGazeExtractor._convert_to_degrees(angle)
                         for angle in [user_roll, user_pitch, user_yaw]])

    @staticmethod
    def _get_roll_pitch_yaw(R):
        return LandmarkGazeExtractor._get_roll_pitch_yaw_gazr(R)

    def _get_detection_descriptor(self,
                                  img: np.array,
                                  detection: ShapeDetection) -> np.array:
        pose = self._get_face_pose(detection, img.shape)

        if pose is None:
            logging.warning('No pose was found.')
            return None

        pose = np.linalg.inv(pose)
        mrot = pose[0:3, 0:3]
        return LandmarkGazeExtractor._get_roll_pitch_yaw(mrot)
