from ..extractor import Extractor, DetectionExtractor
from ...detector import Detection
from typing import List
import torch
from torch.nn import Module
from torch.autograd import Variable
import numpy as np
from PIL import Image
import logging
from torchvision import transforms


class TorchModuleExtractor(Extractor):
    """An ``Extractor`` using trained PyTorch ``nn.Module``s.

    :param Module model: the PyTorch ``nn.Module`` to use.
    :param List[str] imgs: the image paths from which a descriptor needs
    to be extracted.
    :param transform: callable that tranforms the image into the desired format
    to feed into the module.
    """
    def __init__(self, model: Module, imgs: List[str],
                 transform=None):
        super().__init__(imgs)

        if torch.cuda.is_available():
            self.model = model.cuda()

        self.model.train(False)

        for param in self.model.parameters():
            param.requires_grad = False
        self.transform = transform

    def _get_descriptor(self, img: np.array) -> np.array:
        img = Image.fromarray(img)

        if self.transform is not None:
            inputs = self.transform(img)
        else:
            inputs = transforms.ToTensor()(img)

        if torch.cuda.is_available():
            inputs = Variable(inputs[None, ...].cuda())
            return self.model(inputs).data.cpu().numpy()[0]
        else:
            inputs = Variable(inputs[None, ...])
            return self.model(inputs).data.numpy()[0]


class TorchDetectionExtractor(DetectionExtractor, TorchModuleExtractor):
    """An extractor for detections."""
    def __init__(self, model: Module, imgs: List[str],
                 dets: List[List[Detection]], transform=None):
        DetectionExtractor.__init__(self, imgs, dets)
        TorchModuleExtractor.__init__(self, model, imgs, transform)

    def _get_detection_descriptor(self,
                                  img: np.array,
                                  detection: Detection) -> np.array:
        if img.ndim != 3:
            logging.warning('Image has ndim != 3')
            return None

        detection = detection.to_int()
        h, w, _ = img.shape
        detection.x_min = max(detection.x_min, 0)
        detection.x_max = min(detection.x_max, w)
        detection.y_min = max(detection.y_min, 0)
        detection.y_max = min(detection.y_max, h)
        return self._get_descriptor(img[detection.y_min:detection.y_max,
                                        detection.x_min:detection.x_max,
                                        :])

    def _get_descriptor(self, img: np.array) -> np.array:
        return TorchModuleExtractor._get_descriptor(self, img)
